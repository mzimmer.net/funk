/*
 * CC0 1.0 Universal (CC0 1.0) - Public Domain Dedication
 *
 *                                No Copyright
 *
 * The person who associated a work with this deed has dedicated the work to
 * the public domain by waiving all of his or her rights to the work worldwide
 * under copyright law, including all related and neighboring rights, to the
 * extent allowed by law.
 */

enum Super {
  case A(a: String)
  case B(a: String)
}

def debug(s: Super): String = {
  val debugInformation = s match {
    case Super.A(a) => a
    case Super.B(b) => b.toString()
  }
  debugInformation
}