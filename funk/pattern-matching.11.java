/*
 * CC0 1.0 Universal (CC0 1.0) - Public Domain Dedication
 *
 *                                No Copyright
 *
 * The person who associated a work with this deed has dedicated the work to
 * the public domain by waiving all of his or her rights to the work worldwide
 * under copyright law, including all related and neighboring rights, to the
 * extent allowed by law.
 */

public abstract class Super {
    private Super() {
    }

    public static final class A extends Super {
        public final String a;

        public A(String a) {
            this.a = a;
        }
    }

    public static final class B extends Super {
        public final int b;

        public B(int b) {
            this.b = b;
        }
    }

    public static String debug(Super s) {
        final String debugInformation;
        if (s instanceof A) {
            final A a = (A) s;
            debugInformation = a.a;
        } else if (s instanceof B) {
            final B b = (B) s;
            debugInformation = String.valueOf(b.b);
        } else {
            throw new IllegalStateException();
        }
        return debugInformation;
    }
}