/*
 * CC0 1.0 Universal (CC0 1.0) - Public Domain Dedication
 *
 *                                No Copyright
 *
 * The person who associated a work with this deed has dedicated the work to
 * the public domain by waiving all of his or her rights to the work worldwide
 * under copyright law, including all related and neighboring rights, to the
 * extent allowed by law.
 */

sealed trait Super

final case class A(a: String) extends Super

final case class B(b: Int) extends Super

def debug(s: Super): String = {
  val debugInformation: String = s match {
    case A("foobar") => "my special case"
    case A(a) => a
    case B(b) => b.toString
  }
  debugInformation
}