sealed class Super {
    data class A(val a: String) : Super()
    data class B(val b: Int) : Super()
}

fun debug(s: Super): String {
    val debugInformation = when (s) {
        is Super.A -> s.a
        is Super.B -> s.b.toString()
    }
    return debugInformation
}