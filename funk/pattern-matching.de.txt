Pattern Matching ist die Idee verschiedene Fällt zu unterscheiden und den ersten treffer zu finden und zu behandeln. Zu jedem Fall gehört ein Ausführungspfad und alle Pfade haben den gleichen Ergebnistypen.

Das ganze gewinnt an Wert in Kombination mit Totalität. Totalität bedeutet hier, dass alle Fälle abgedeckt sind und somit sicher gestellt werden kann, dass ein Ergebnis gefunden wird. Das impliziert, dass der Kompiler den Entwickler warnen kann, wenn ein Fall übersehen wird.

Vollständiges Pattern Matching beinhaltet nicht die die Unterscheidung von Typen sondern auch von Werten.