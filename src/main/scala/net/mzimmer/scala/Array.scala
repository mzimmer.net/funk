package net.mzimmer.scala

object Array {

  implicit class ArrayOps[T](array: Array[T]) {
    def toImmutableSeq: scala.collection.immutable.Seq[T] = scala.collection.immutable.Seq(array: _*)
  }

}
