package net.mzimmer.scala

object Map {

  implicit class MapOps[K, V](map: scala.collection.immutable.Map[K, V]) {
    def addedIfMissingAndUpdated(key: K)(f: => V)(g: V => V): scala.collection.immutable.Map[K, V] =
      map.updated(key, map.get(key) match {
        case Some(value) => g(value)
        case None        => g(f)
      })
  }

}
