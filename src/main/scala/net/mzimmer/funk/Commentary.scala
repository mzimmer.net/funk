package net.mzimmer.funk

import java.io.File

final case class Commentary(file: File)
