package net.mzimmer.funk

import scala.collection.immutable.Map

final case class Problem(
    problemDescriptions: Map[Language, ProblemDescription],
    solutions: Map[SolutionId, Solution]
)

object Problem {
  val identity: Problem = Problem(Map.empty, Map.empty)
}
