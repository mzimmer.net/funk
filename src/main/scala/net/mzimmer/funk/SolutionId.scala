package net.mzimmer.funk

final case class SolutionId(programmingLanguage: ProgrammingLanguage, designator: Option[Designator])
