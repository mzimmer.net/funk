package net.mzimmer.funk

import cats.implicits._
import monocle.macros.syntax.lens._
import net.mzimmer.scala.Map.MapOps

import scala.collection.immutable._

final case class Problems(problems: Map[ProblemId, Problem])

object Problems {
  val identity: Problems = Problems(Map.empty)

  def fromResources(resources: Seq[Resource]): Problems =
    resources.foldLeft(Problems.identity) { (problems, resource) =>
      problems
        .lens(_.problems)
        .modify(
          _.addedIfMissingAndUpdated(resource.problemId)(Problem.identity)(
            problem =>
              resource match {
                case Resource.ProblemDescriptionResource(file, _, language) =>
                  problem.lens(_.problemDescriptions).modify(_ + (language -> ProblemDescription(file)))

                case solutionResource: Resource.SolutionResource =>
                  problem
                    .lens(_.solutions)
                    .modify(
                      _.addedIfMissingAndUpdated(solutionResource.solutionId)(Solution.identity)(
                        solution =>
                          solutionResource match {
                            case Resource.SourceCodeResource(file, _, _) =>
                              solution.lens(_.sourceCode).modify(_ => SourceCode(file).some)

                            case Resource.CommentaryResource(file, _, _, language) =>
                              solution.lens(_.commentaries).modify(_.updated(language, Commentary(file)))
                          }
                      )
                    )
              }
          )
        )
    }
}
