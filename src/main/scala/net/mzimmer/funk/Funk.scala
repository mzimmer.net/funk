package net.mzimmer.funk

import java.io.File

import cats.effect.{ ExitCode, IO, IOApp }
import net.mzimmer.scala.Array.ArrayOps

import scala.collection.immutable._

object Funk extends IOApp {
  override def run(args: List[String]): IO[ExitCode] =
    for {
      resourcesRoot <- IO(new File("funk"))
      files         <- IO(resourcesRoot.listFiles().toImmutableSeq)
      exitCode <- Resource.multipleFromFile(files) match {
        case Left(files)           => handleInvalidResources(files)
        case Right(validResources) => handleValidResources(validResources)
      }
    } yield {
      exitCode
    }

  def handleInvalidResources(files: Seq[File]): IO[ExitCode] =
    IO {
      println(s"The following resource files have invalid file names: ${files.map(_.getName).mkString(", ")}")
      ExitCode.Error
    }

  def handleValidResources(resources: Seq[Resource]): IO[ExitCode] = {
    val problems = Problems.fromResources(resources)
    IO {
      println(problems)
      ExitCode.Success
    }
  }
}
