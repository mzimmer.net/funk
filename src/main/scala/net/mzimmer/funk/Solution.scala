package net.mzimmer.funk

import scala.collection.immutable.Map

final case class Solution(sourceCode: Option[SourceCode], commentaries: Map[Language, Commentary])

object Solution {
  val identity: Solution = Solution(None, Map.empty)
}
