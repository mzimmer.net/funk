package net.mzimmer.funk

import java.io.File

import cats.implicits._

import scala.collection.immutable._

sealed trait Resource {
  def file: File

  def problemId: ProblemId
}

object Resource {

  final case class ProblemDescriptionResource(
      file: File,
      problemId: ProblemId,
      language: Language
  ) extends Resource

  sealed trait SolutionResource extends Resource {
    def solutionId: SolutionId
  }

  final case class SourceCodeResource(
      file: File,
      problemId: ProblemId,
      solutionId: SolutionId
  ) extends SolutionResource

  final case class CommentaryResource(
      file: File,
      problemId: ProblemId,
      solutionId: SolutionId,
      language: Language
  ) extends SolutionResource

  def fromFile(file: File): Option[Resource] =
    file.getName.split('.').toList match {

      case problemId :: designator :: programmingLanguageFileExtension :: language :: "txt" :: Nil =>
        val solutionId = SolutionId(ProgrammingLanguage(programmingLanguageFileExtension), Designator(designator).some)
        CommentaryResource(file, ProblemId(problemId), solutionId, Language(language)).some

      case problemId :: programmingLanguageFileExtension :: language :: "txt" :: Nil =>
        val solutionId = SolutionId(ProgrammingLanguage(programmingLanguageFileExtension), None)
        CommentaryResource(file, ProblemId(problemId), solutionId, Language(language)).some

      case problemId :: language :: "txt" :: Nil =>
        ProblemDescriptionResource(file, ProblemId(problemId), Language(language)).some

      case problemId :: designator :: programmingLanguageFileExtension :: Nil =>
        val solutionId = SolutionId(ProgrammingLanguage(programmingLanguageFileExtension), Designator(designator).some)
        SourceCodeResource(file, ProblemId(problemId), solutionId).some

      case problemId :: programmingLanguageFileExtension :: Nil =>
        val solutionId = SolutionId(ProgrammingLanguage(programmingLanguageFileExtension), None)
        SourceCodeResource(file, ProblemId(problemId), solutionId).some

      case _ => None
    }

  def multipleFromFile(files: Seq[File]): Either[Seq[File], Seq[Resource]] =
    files
      .map(file => file -> Resource.fromFile(file))
      .foldLeft(Either.right[Seq[File], Seq[Resource]](Seq.empty)) { (resourcesOrError, fileAndMaybeResource) =>
        (resourcesOrError, fileAndMaybeResource) match {
          case (Left(files), (file, None))             => Left(files :+ file)
          case (Left(files), (_, Some(_)))             => Left(files)
          case (Right(_), (file, None))                => Left(Seq(file))
          case (Right(resources), (_, Some(resource))) => Right(resources :+ resource)
        }
      }
}
